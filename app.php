<?php

require __DIR__ . '/vendor/autoload.php';

use config\Config;
use services\DataLoader;
use models\Transaction;

$rateText = DataLoader::getCurl(Config::API_RATES);
$rates = json_decode($rateText);
$rates = $rates->rates ?? null;
if (!$rates) {
  throw new \Exception('There is no rates', 0001);
}

foreach (explode("\n", file_get_contents($argv[1])) as $row) {
  $transaction = new Transaction($row);
  if (!$transaction->isValid()) {
    print "error!\n";
    continue;
  }

  if (!$transaction->getBankInfo()) {
    print "error!\n";
    continue;

  }

  $isEurope = $transaction->isEurope();
  $rate = $rates->{$transaction->getCurrency()} ?? 0;

  $amntFixed = $transaction->getAmount();
  if ($transaction->getCurrency() !== 'EUR' && $rate > 0) {
    $amntFixed = $amntFixed / $rate;
  }

  echo ceil($amntFixed * ($isEurope ? 0.01 : 0.02) * 100) / 100;
  print "\n";
}