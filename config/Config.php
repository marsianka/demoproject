<?php
namespace config;

class Config {
  const API_RATES = 'https://api.exchangeratesapi.io/latest';
  const API_BINLIST = 'https://lookup.binlist.net/';
  const API_GEO = 'https://restcountries.eu/rest/v2/alpha/';
}