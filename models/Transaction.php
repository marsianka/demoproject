<?php

namespace models;

use config\Config;
use services\DataLoader;

/**
 * Class Transaction
 *  @property $bin
 *  @property $amount
 *  @property $currency
 *  @property $bankInfo
 *  @property $isEurope
 */
class Transaction {
  protected $bin = '';
  protected $amount = 0;
  protected $currency = '';

  protected $bankInfo = null;
  protected $isEurope = null;

  /**
   * Transaction constructor. Fill transaction from json data
   * @param string $json
   */
  public function __construct($json)
  {
    $transaction = json_decode($json, true);

    if ($transaction) {
      $fields = $this->inputFields();
      foreach ($fields as $key) {
        $this->$key = $transaction[$key] ?? null;
      }
    }
  }

  /**
   * @return string[] Array of fields filled in constructor
   */
  public function inputFields() {
    return ['bin', 'amount', 'currency'];
  }

  public function getCurrency() {
    return $this->currency;
  }

  public function getAmount() {
    return $this->amount;
  }

  public function getBin() {
    return $this->bin;
  }

  /**
   * Check is it valid transaction
   * @TODO Add some validation rules
   * @return bool
   */
  public function isValid() {
    return !empty($this->bin) && is_numeric($this->amount) && $this->amount > 0 && !empty($this->currency);
  }

  /**
   * Load bank information from API
   */
  public function getBankInfo() {
    if (!empty($this->bankInfo)) {
      return $this->bankInfo;
    }
    $data = DataLoader::getCurl(Config::API_BINLIST . $this->bin);
    $this->bankInfo = json_decode($data);

    return $this->bankInfo;
  }

  /**
   * Check is transaction from europe bank
   * @return bool
   * @throws \Exception
   */
  public function isEurope() {
    if (!is_null($this->isEurope)) {
      return $this->isEurope;
    }

    $bankInfo = $this->getBankInfo();

    $countryCode = $bankInfo->country->alpha2 ?? null;

    if (!$countryCode) {
      return false;
    }

    /**
     * @TODO Replace API with google API?
     */
    if ($countryCode == 'GB') {
      return false;
    }

    /**
     * @TODO Cache the results, don't send requests for the same countries
     */
    $data = DataLoader::getCurl(Config::API_GEO . $countryCode);
    $geo = json_decode($data);
    $region = $geo->region ?? null;

    $this->isEurope = ($region == 'Europe');

    return $this->isEurope;
  }
}