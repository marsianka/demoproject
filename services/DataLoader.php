<?php

namespace services;

/**
 * Class DataLoader take the data from some sources
 * @package services
 */
class DataLoader {

  /**
   * Load data from URL
   * @param $url
   * @return bool|string
   * @throws \Exception
   */
  public static function getCurl($url) {
    $headers = array();
    $headers[] = "Content-Type: application/json";
    $headers[] = "Cache-Control: no-cache";

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $data = curl_exec($ch);
    $error = curl_error($ch);
    curl_close($ch);

    if ($error) {
      throw new \Exception($error, 0002);
    }

    return $data;
  }
}