<?php

namespace tests;

use models\Transaction;
use PHPUnit\Framework\TestCase;

class TransactionTest extends TestCase {

  protected function getValidData() {
    return [
      "100.00" => '{"bin":"45717360","amount":"100.00","currency":"EUR"}',
      "50.00" => '{"bin":"516793","amount":"50.00","currency":"USD"}',
      "10000.00" => '{"bin":"45417360","amount":"10000.00","currency":"JPY"}',
    ];
  }

  protected function getInValidData() {
    return [
      '{"bin":"45717360","amount":"test","currency":"EUR"}',
      '{"bin":"516793","amount":"50.00","currency":""}',
      '{"bin":"","amount":"150.00","currency":"RUB"}',
      'test test',
    ];
  }

  public function testCreate()
  {
    $jsonList = $this->getValidData();
    foreach ($jsonList as $value => $json) {
      $transaction = new Transaction($json);
      $this->assertEquals($value, $transaction->getAmount());
    }
  }

  public function testIsValid() {
    $jsonList = $this->getValidData();
    foreach ($jsonList as $value => $json) {
      $transaction = new Transaction($json);
      $this->assertEquals(true, $transaction->isValid());
    }
  }

  public function testIsInvalid() {
    $jsonList = $this->getInValidData();
    foreach ($jsonList as $value => $json) {
      $transaction = new Transaction($json);
      $this->assertEquals(false, $transaction->isValid());
    }
  }

  public function testIsEurope() {
    $json = '{"bin":"45717360","amount":"100.00","currency":"EUR"}';
    $transaction = new Transaction($json);
    $this->assertEquals(true, $transaction->isEurope());
  }

  public function testNotIsEurope() {
    $json = '{"bin":"45417360","amount":"100.00","currency":"EUR"}';
    $transaction = new Transaction($json);
    $this->assertEquals(false, $transaction->isEurope());
  }

}